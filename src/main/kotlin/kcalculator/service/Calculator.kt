package kcalculator.service

interface Calculator {

    fun add(vararg args: Int): Int

    fun sub(vararg args: Int): Int
}