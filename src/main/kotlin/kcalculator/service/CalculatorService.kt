package kcalculator.service

import javax.inject.Singleton

@Singleton
class CalculatorService : Calculator {
    override fun add(vararg args: Int) = args.reduce { acc: Int, d: Int -> acc + d }

    override fun sub(vararg args: Int) = args.reduce { acc: Int, d: Int -> acc - d }

}