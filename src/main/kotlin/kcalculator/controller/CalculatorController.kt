package kcalculator.controller

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import kcalculator.service.Calculator
import org.slf4j.LoggerFactory
import javax.inject.Inject


@Controller("/")
open class CalculatorController {

    @Inject
    private lateinit var calculator: Calculator

    private val LOG = LoggerFactory.getLogger(CalculatorController::class.java)


    @Get("{a}+{b}")
    @Produces(MediaType.APPLICATION_JSON)
    open fun add(@PathVariable a: Int, @PathVariable b: Int): CalculatorResponse {
        LOG.info("{} + {}", a, b)
        val result = calculator.add(a, b)
        LOG.info("= {}", result)
        return CalculatorResponse(result)
    }

    @Get("{a}-{b}")
    @Produces(MediaType.APPLICATION_JSON)
    open fun subtract(@PathVariable a: Int, @PathVariable b: Int): CalculatorResponse {
        LOG.info("{} - {}", a, b)
        val result = calculator.sub(a, b)
        LOG.info("= {}", result)
        return CalculatorResponse(result)
    }

    @Post("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    open fun add(@Body body: CalculatorRequest): CalculatorResponse {
        LOG.info("add {}", body.numbers)
        val result = calculator.add(*body.numbers)
        LOG.info("{}", result)
        return CalculatorResponse(result)
    }


    @Post("/sub")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    open fun sub(@Body body: CalculatorRequest): CalculatorResponse {
        LOG.info("sub {}", body.numbers)
        val result = calculator.sub(*body.numbers)
        LOG.info("{}", result)
        return CalculatorResponse(result)
    }
}
