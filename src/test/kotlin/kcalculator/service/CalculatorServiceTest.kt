package kcalculator.service

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class CalculatorServiceTest {

    private val service = CalculatorService()

    @Test
    fun shouldAdd() {
        // when
        val result = service.add(2, 3)

        // then
        assertThat(result).isEqualTo(5)
    }

    @Test
    fun shouldSub() {
        // when
        val result = service.sub(2, 3)

        // then
        assertThat(result).isEqualTo(-1)
    }
}