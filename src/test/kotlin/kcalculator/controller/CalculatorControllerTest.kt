package kcalculator.controller

import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

@MicronautTest
class CalculatorControllerTest {
    private val embeddedServer: EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
    private val client: HttpClient = HttpClient.create(embeddedServer.url)

    @Test
    fun shouldAdd() {
        // when
        val response = client.toBlocking().retrieve("/1+1", CalculatorResponse::class.java)

        // then
        assertThat(response.result).isEqualTo(2)
    }

    @Test
    fun shouldSub() {
        // when
        val response = client.toBlocking().retrieve("/1-2", CalculatorResponse::class.java)

        // then
        assertThat(response.result).isEqualTo(-1)
    }

    @Test
    fun shouldAddMultiple() {
        // given
        val request = CalculatorRequest(intArrayOf(2, 3, -2, 1))

        // when
        val response = client.toBlocking().exchange(
                HttpRequest.POST("/add", request),
                CalculatorResponse::class.java
        )

        // tehn
        assertThat(response.body()!!.result).isEqualTo(4)
    }

    @Test
    fun shouldSubtractMultiple() {
        // given
        val request = CalculatorRequest(intArrayOf(2, 3, -2, 1))

        // when
        val response = client.toBlocking().exchange(
                HttpRequest.POST("/sub", request),
                CalculatorResponse::class.java
        )

        // tehn
        assertThat(response.body()!!.result).isEqualTo(0)
    }
}