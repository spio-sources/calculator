# Calculator
Prosta implementacja kalkulatora w postaci web aplikacji backendowej o jasno zdefiniowanym API. Aplikacja przegenerowuje i udostępnia w formie zasobu statycznego specyfikację API (plik yaml), korzystajć z biblioteki Swagger i specyfikacji OpenAPI.

Aplikacja została zaimplementowana w jednym języku, należącym do ekosystemu Java. Cały projekt jest kompletną, uruchamialną aplikacją.

## Technologie i projekty
### kcalculator
Kotlin, Micronaut, Swagger, JDK 11, Docker, JUnit

## Uwagi techniczne
### Preferowane IDE
IntelliJ - za IDE i Kotlinem stoi jedna firma: JetBrains
### Budowanie aplikacji
`mvn clean install`
### Uruchamianie testów
Z wykorzystaniem IDE lub jw.
### Uruchamianie aplikacji
Z wykorzystaniem IDE (przez main-klasę) lub po zbudowaniu:

`java -jar target/kcalculator-0.1.jar`

Operacje są dostępne jako kolejne endpointy:
 - GET:
   - http://localhost:8080/1+3
   - http://localhost:8080/3-5
 - POST
   - curl -d '{"numbers": [2, 3, 4]}' -H "Content-Type: application/json" -X POST http://localhost:8080/add
   - curl -d '{"numbers": [2, 3, 4]}' -H "Content-Type: application/json" -X POST http://localhost:8080/sub
 - osobno specyfikacja API: http://localhost:8080/swagger/kcalculator-0.0.yml
### Docker
Projekt zawiera Dockerfile oraz docker-compose.yml. Specyfikacja dockerowa tworzy obraz na bazie linuksa z Adopt JDK, kopiuje i uruchamia plik jar.

Uruchamianie:
`docker-compose up`

Adresy jak w poprzedniej sekcji, oprócz Windowsa, tu należy się spodziewać np. http://192.168.99.100:8080/swagger/kcalculator-0.0.yml